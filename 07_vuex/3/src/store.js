import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  searches: [
  ],
  favoriteSearches: [
  ]
};
const getters = {
  searchesStr (state) {
    return state.searches.join(', ');
  },
  isSearchFavorite (state) {
    return (search) => {
      return state.favoriteSearches.includes(search);
    };
  },
  searchesWithFavorites (state) {
    return state.searches.map(s => {
      return {
        text: s,
        isFavorite: state.favoriteSearches.includes(s)
      };
    });
  }
};

const mutations = {
  addSearch (state, search) {
    state.searches.unshift(search);
  },

  addFavoriteSearch (state, search) {
    state.favoriteSearches.push(search);
  },

  removeSearch (state, index) {
    state.searches.splice(index, 1);
  },

  clearSearches (state) {
    state.searches = [];
  }
};
const actions = {
  addSearch ({ state, commit }, search) {
    const foundIndex = state.searches.indexOf(search);
    if (foundIndex > -1) {
      commit('removeSearch', foundIndex);
    }
    commit('addSearch', search);
  }
};

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
});
