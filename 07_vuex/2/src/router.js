import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Serp from './views/Serp.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/serp',
      name: 'serp',
      component: Serp
    }
  ]
});
