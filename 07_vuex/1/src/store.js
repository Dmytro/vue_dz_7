import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  searches: [
  ]
};
const mutations = {
  addSearch (state, search) {
    state.searches.unshift(search);
  },

  removeSearch (state, index) {
    state.searches.splice(index, 1);
  },

  clearSearches (state) {
    state.searches = [];
  }
};
const actions = {
  // addSearch0 (context, search) {
  //   const foundIndex = context.state.searches.indexOf(search);
  //   if (foundIndex > -1) {
  //     context.commit('removeSearch', foundIndex);
  //   }
  //   context.commit('addSearch', search);
  // },
  addSearch ({ state, commit }, search) {
    // const foundIndex = state.searches.findIndex(s => s === search);
    const foundIndex = state.searches.indexOf(search);
    if (foundIndex > -1) {
      commit('removeSearch', foundIndex);
    }
    commit('addSearch', search);
  }
};

// export default new Vuex.Store({
//   state: {
//   },
//   mutations: {
//   },
//   actions: {
//   }
// });

export default new Vuex.Store({
  state,
  mutations,
  actions
});
