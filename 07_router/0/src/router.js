import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import About from './views/About.vue';
// import AboutFooter from './views/AboutFooter.vue';

const AboutFooter = {
  template: '<footer>FOOTER</footer>'
};

Vue.use(Router);

export default new Router({
  mode: 'history',
  // base: process.env.BASE_URL,
  // base: '/',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // component: About
      components: {
        default: About,
        first: AboutFooter
      }
    }
  ]
});
