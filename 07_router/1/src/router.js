import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Subpage from './views/Subpage.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      beforeEnter (to, from, next) {
        console.log('About before Enter');
        next()
      }
    },
    {
      path: '/:subpage',
      name: Subpage,
      component: Subpage
    }
  ]
})
