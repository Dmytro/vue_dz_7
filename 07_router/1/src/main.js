import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  console.log('beforeEach');
  // console.log(to);
  // console.log(from);
  next()

  // if (to.name === 'about') {
  //   next('/subpage');
  // } else {
  //   next()
  // }
})

router.beforeResolve((to, from, next) => {
  console.log('beforeResolve');
  next();
})


new Vue({
  router,
  render: h => h(App),
  watch: {
    $route (to, from) {
      console.log('WATCH', to);
      console.log('WATCH', from);
    }
  }
}).$mount('#app')
